// Allan CORNET - 2010 - 2011

function builder_gw()

  sci_gateway_dir = get_absolute_file_path("builder_gateway.sce");
  languages       = ["c"];

  tbx_builder_gateway_lang(languages,sci_gateway_dir);
  tbx_build_gateway_loader(languages,sci_gateway_dir);
  tbx_build_gateway_clean(languages,sci_gateway_dir);

endfunction

builder_gw();
clear builder_gw;

