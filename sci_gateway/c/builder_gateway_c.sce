// Allan CORNET - 2010 - 2011

function builder_gateway_c()

if getos() == "Windows" then
    // to manage long pathname
    includes_src_c = "-I""" + get_absolute_file_path("builder_gateway_c.sce") + "../../src/c""";
else
    includes_src_c = "-I" + get_absolute_file_path("builder_gateway_c.sce") + "../../src/c";
end

// PutLhsVar managed by user in sci_sum and in sci_sub
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %t;

files_functions = ["sci_SMP_affinity.c", ..
                   "sci_SMP_priority.c"];

name_functions = ["SMP_affinity", "sci_SMP_affinity"; ..
                  "SMP_priority","sci_SMP_priority"];

tbx_build_gateway("SMP", ..
                  name_functions, ..
                  files_functions, ..
                  get_absolute_file_path("builder_gateway_c.sce"), ..
                  "../../src/c/libSMP_core", ..
                  "", ..
                  includes_src_c);

endfunction

builder_gateway_c();
clear builder_gateway_c;
