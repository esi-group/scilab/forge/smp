/* ==================================================================== */
/* Allan CORNET - 2010 */
/* ==================================================================== */
#include <stdlib.h>
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "SMP_affinity.h"
#include "MALLOC.h"
/* ==================================================================== */
static int sci_SMP_setaffinity(char *fname);
static int sci_SMP_getaffinity(char *fname);
/* ==================================================================== */
int sci_SMP_affinity(char *fname)
{
    Rhs = Max(0, Rhs);
    CheckRhs(0, 1);
    CheckLhs(1, 1);

    if (Rhs == 0)
    {
        return sci_SMP_getaffinity(fname);
    }
    else
    {
        return sci_SMP_setaffinity(fname);
    }
    return 0;
}
/* ==================================================================== */
static int sci_SMP_setaffinity(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;
    int m1 = 0, n1 = 0;
    int iType1  = 0;
    int nbCPUs = SMP_getNumberOfCpus();

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (iType1 != sci_boolean)
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: Vector of booleans expected.\n"), fname, 1);
        return 0;
    }

    sciErr = getVarDimension(pvApiCtx, piAddressVarOne, &m1, &n1);

    if ( ((m1 == 1) && (n1 == nbCPUs)) || ((m1 == nbCPUs) && (n1 == 1)) )
    {
        int i = 0;
        int almostOneCPU = 0;
        int *iDatas = NULL;
        sciErr = getMatrixOfBoolean(pvApiCtx, piAddressVarOne, &m1, &n1, &iDatas);
        if(sciErr.iErr)
        {
            FREE(iDatas);
            iDatas = NULL;
            printError(&sciErr, 0);
            return 0;
        }

        if (iDatas == NULL)
        {
            Scierror(999,_("%s: No more memory.\n"), fname);
            return 0;
        }

        /* checks that at least one CPU must be ON in new config */
        for (i = 0; i < nbCPUs; i++)
        {
            if (iDatas[i] != 0) almostOneCPU++;
        }

        if (almostOneCPU)
        {
            SMP_setScilabCpuAffinity(iDatas, m1 * n1);
            iDatas = NULL;
        }
        else
        {
            iDatas = NULL;
            Scierror(999,_("%s: Wrong value for input argument #%d: at least a value must be true.\n"), fname, 1);
            return 0;
        }
        return sci_SMP_getaffinity(fname);
    }
    else
    {
        Scierror(999,_("%s: Wrong size for input argument #%d: Vector of booleans expected.\n"), fname, 1);
    }

    return 0;
}
/* ==================================================================== */
static int sci_SMP_getaffinity(char *fname)
{
    SciErr sciErr;
    int sizeAffinityMask = 0;
    int *affinityMask = NULL;

    affinityMask = SMP_getScilabCpuAffinity(&sizeAffinityMask);
    if ((affinityMask == NULL) || (sizeAffinityMask <= 0))
    {
        Scierror(999,_("%s: Wrong value returned.\n"), fname);
        return 0;
    }

    sciErr = createMatrixOfBoolean(pvApiCtx, Rhs + 1, 1,sizeAffinityMask, affinityMask);

    FREE(affinityMask);
    affinityMask = NULL;

    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
    return 0;
}
/* ==================================================================== */
