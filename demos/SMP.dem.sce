// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - Allan CORNET
//

function demo_SMP()

  mode(-1);
  lines(0);
  
  disp("Numbers of CPUs: " + string(SMP_getCpusCount()));
  disp("Get CPUs Affinity: " + strcat(string(SMP_affinity())," "));
  disp("Get Scilab Priority Process: " + string(SMP_priority()));

endfunction


demo_SMP();
clear demopath;
