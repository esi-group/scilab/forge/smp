//------------------------------------------------------------------------------
// Allan CORNET - 2010
//------------------------------------------------------------------------------
#include <windows.h>
#include "SMP_affinity.h"
#include "MALLOC.h"
//------------------------------------------------------------------------------
static int bin2dec(int *bin, int sizebin);
static int *dec2bin(int dec, int width);
//------------------------------------------------------------------------------
int *SMP_getScilabCpuAffinity(int *sizeArrayReturned)
{
    int *affinityArray = NULL;

    *sizeArrayReturned = SMP_getNumberOfCpus();

    if (*sizeArrayReturned)
    {
        DWORD_PTR process_cpus = 0;
        DWORD_PTR system_cpus = 0;
        int i = 0;

        if (GetProcessAffinityMask (GetCurrentProcess (), &process_cpus, &system_cpus))
        {
            int *invArray = dec2bin(process_cpus, *sizeArrayReturned);

            if (invArray == NULL) 
            {
                *sizeArrayReturned = 0;
                return affinityArray = NULL;
            }

            affinityArray = (int*)MALLOC(sizeof(int) * (*sizeArrayReturned));
            if (affinityArray == NULL) 
            {
                FREE(invArray);
                invArray = NULL;

                *sizeArrayReturned = 0;
                return affinityArray = NULL;
            }

            for (i = 0; i < *sizeArrayReturned; i++)
            {
                affinityArray[i] = invArray[*sizeArrayReturned - i - 1];
            }

            FREE(invArray);
            invArray = NULL;
        }
        else
        {
            for (i = 0; i < *sizeArrayReturned; i++)
            {
                affinityArray[i] = 1;
            }
        }
    }
    else
    {
        *sizeArrayReturned = 0;
    }
    return affinityArray;
}
//------------------------------------------------------------------------------
int SMP_setScilabCpuAffinity(const int *affinityArray, int sizeAffinityArray)
{
    if (sizeAffinityArray == SMP_getNumberOfCpus())
    {
        int i = 0;
        DWORD newMask = 0;
        int *invArray = (int*)MALLOC(sizeof(int) * sizeAffinityArray);
        if (invArray == NULL) return 0;

        for (i = 0; i < sizeAffinityArray; i++)
        {
            invArray[i] = affinityArray[sizeAffinityArray -1 - i];
        }
        newMask = (DWORD)bin2dec(invArray, sizeAffinityArray);

        FREE(invArray);
        invArray = 0;

        if (!SetProcessAffinityMask(GetCurrentProcess(), newMask))
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    return 0;
}
//------------------------------------------------------------------------------
int SMP_getNumberOfCpus(void)
{
    DWORD_PTR process_cpus = 0;
    DWORD_PTR system_cpus = 0;

    if (GetProcessAffinityMask (GetCurrentProcess(), &process_cpus, &system_cpus))
    {
        DWORD_PTR bit = 0;
        int CPUs = 0;

        for (bit = 1; bit != 0; bit <<= 1)
        {
            if (system_cpus & bit)
            {
                CPUs++;
            }
        }
        return CPUs;
    }
    return 1;
}
//------------------------------------------------------------------------------
static int bin2dec(int *bin, int sizebin)
{
    int dec = 0;
    int i = 0;
    for (i = 0; i < sizebin; i++)
    {
        dec = dec * 2 + bin[i];
    }
    return dec;
}
//------------------------------------------------------------------------------
static int *dec2bin(int dec, int width)
{
    int *bits = (int*)MALLOC(width * sizeof(int));
    if (bits)
    {
        int i = 0;
        int rest = 1;

        for(i = (width-1); rest != 0; i--)
        {
            rest = dec / 2;
            bits[i] = (dec % 2);
            dec = rest;
        }
    }
    return bits;
}
//------------------------------------------------------------------------------
