// Allan CORNET - 2010 - 2011

function builder_c()

  src_c_path = get_absolute_file_path("builder_c.sce");

  CFLAGS = "-I" + src_c_path;

  tbx_build_src("SMP_core",    ..
              ["SMP_affinity.c", "SMP_priority.c"],..
              "c", ..             ..
              src_c_path,         ..
              "",                 ..
              "",                 ..
              CFLAGS);
endfunction

builder_c();
clear builder_c;