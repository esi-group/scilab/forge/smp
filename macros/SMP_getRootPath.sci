// ====================================================================
// Allan CORNET - 2010
// ====================================================================
function p = SMP_getRootPath()
  [m, p] = libraryinfo("SMPlib");
  p = fullpath(p + "../");
endfunction
// ====================================================================
