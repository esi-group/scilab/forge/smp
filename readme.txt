readme.txt of the SMP

SMP - 0.3 
  Compatibility with Scilab 5.4

SMP - 0.2 
  fix a memory leak on Windows x64 bits with a 16 cpus core

SMP - 0.1 (initial version)
  SMP_affinity - get/set cpu affinity of Scilab process
  SMP_priority - get/set Scilab priority process
  SMP_getCpusCount - get Cpus count


Author: Allan CORNET 